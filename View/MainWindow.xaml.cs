﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using RxRelatorioPreco.Model;
using RxRelatorioPreco.Repository;
using System.Linq;
using System;
using RxRelatorioPreco.SeedControl;

namespace RxRelatorioPreco.View
{

    public partial class MainWindow
    {
        public static object Response { get; private set; }

        public static ArquivoFuncoes _arquivoFunctions = new ArquivoFuncoes();

        public static ProdutoFuncoes _produtoFunctions = new ProdutoFuncoes();

        private static bool _liberaAtualizacao = false;

        public MainWindow()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            InitializeComponent();
            new SplashWindow().ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ShowFilial();
            ShowSituacaoProd();
            ShowEstoque();
            ShowOrdenacao();
            ShowFornecedor(null);
            ShowOrigemPed();

            _liberaAtualizacao = true;
            Mouse.OverrideCursor = null;
            LiberaItemTela(true);
        }

        private void CmdGera_Click(object sender, RoutedEventArgs e)
        {

            if (Validacao() != "") { TxtLog.Text = Validacao(); return; }

            Mouse.OverrideCursor = Cursors.Wait;
            var produtoRepository = new ProdutoRepository();
        
            var count = 1;

            TxtLog.Text = "Buscando os produtos da condição\n" + TxtLog.Text;
            System.Windows.Forms.Application.DoEvents();
            try
            {
                List<Produto> produtos = new List<Produto>();
                if (((Produto)CmbProduto.SelectedValue).ProdutoCodigo != null)
                {


                    var produto = produtoRepository.BuscaProdutoById(
                                                                     ((Produto)CmbProduto.SelectedValue).ProdutoCodigo,
                                                                     ((Filial)CmbFilial.SelectedValue).FilialCodigo,
                                                                     ((Condicao)CmbCondicao.SelectedItem).CondicaoCodigo
                                                                     );
                    produtos.Add(produto);

                    foreach (var prod in produtos)
                    {
                        produto = _produtoFunctions.MontaProduto(prod,
                                                                    ((Cliente)CmbCliente.SelectedValue).ClienteCodigo,
                                                                    ((Filial)CmbFilial.SelectedValue).FilialCodigo,
                                                                    ((KeyValuePair<int, string>)CmbOrigemPed.SelectedItem).Key,
                                                                    ((Condicao)CmbCondicao.SelectedItem).CondicaoId,
                                                                    ((Filial)CmbFilial.SelectedValue).UF
                                                                    );

                        TxtLog.Text = TxtLog.Text + $"O produto {produto.ProdutoCodigo}{produto.Digito} está sendo processado!({count} de 1) \n" + TxtLog.Text;
                        System.Windows.Forms.Application.DoEvents();
                        count++;
                    }

                }
                else
                {
                    produtos = _produtoFunctions.BuscaProdutos(
                                                    ((Condicao)CmbCondicao.SelectedValue).CondicaoCodigo,
                                                    ((Filial)CmbFilial.SelectedValue).FilialCodigo,
                                                    ((Fornecedor)CmbLaboratorio.SelectedValue).FornecedorCodigo,
                                                    ((KeyValuePair<int, string>)CmbSituacaoProd.SelectedItem).Key,
                                                    ((KeyValuePair<int, string>)CmbEstoque.SelectedItem).Key
                                                     ).Where(p => p.ProdutoCodigo != null)
                                                     .ToList();


                    foreach (var produto in produtos)
                    {

                        var prod = _produtoFunctions.MontaProduto(produto,
                                                        ((Cliente)CmbCliente.SelectedValue).ClienteCodigo,
                                                        ((Filial)CmbFilial.SelectedValue).FilialCodigo,
                                                        ((KeyValuePair<int, string>)CmbOrigemPed.SelectedItem).Key,
                                                        ((Condicao)CmbCondicao.SelectedItem).CondicaoId,
                                                        ((Filial)CmbFilial.SelectedValue).UF
                                                        );

                        produto.TributacaoItem = prod.TributacaoItem;

                        TxtLog.Text = $"O produto {produto.ProdutoCodigo}{produto.Digito} está sendo processado! ({count} de {produtos.Count()})\n" + TxtLog.Text;
                        System.Windows.Forms.Application.DoEvents();
                        count++;
                    }
                    produtos = _produtoFunctions.OrdenaProdutoList(produtos, ((KeyValuePair<int, string>)CmbOrdenacao.SelectedItem).Key).ToList();
                }
                TxtLog.Text = $"Gravando inforamções no arquivo! Por favor, aguarde.\n" + TxtLog.Text;
                _arquivoFunctions.ProcessaArquivo(produtos);
                TxtLog.Text = $"Arquivo processado com sucesso! \n" + TxtLog.Text;
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                TxtLog.Text = "Não foi possível finalizar o processo. Analise o erro a seguir: " + error + $"\n" + TxtLog.Text;
                return;
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }

        private void ShowCliente(string cliente)
        {
            ClienteRepository clienteRepository = new ClienteRepository();
            CmbCliente.Items.Add(clienteRepository.Obter(cliente));

            if (CmbCliente.Items.Count > 0) CmbCliente.SelectedIndex = 0;
        }

        private void ShowFilial()
        {
            FilialRepository filialRepository = new FilialRepository();
            CmbFilial.ItemsSource = filialRepository.ObterTodos();


            if (CmbFilial.Items.Count > 0) CmbFilial.SelectedIndex = 0;
        }

        private void ShowCondicao()
        {
            if (CmbCliente.SelectedIndex < 0 || CmbCliente.SelectedIndex < 0) return;

            Mouse.OverrideCursor = Cursors.Wait;

            CondicaoRepository condicaoRepository = new CondicaoRepository();
            var clienteCodigo = ((Cliente)CmbCliente.SelectedValue).ClienteCodigo;


            var condicoes = condicaoRepository.SqlObterPorEntidade(clienteCodigo,
                                                ((KeyValuePair<int, string>)CmbOrigemPed.SelectedItem).Key);
            CmbCondicao.ItemsSource = condicoes;

            if (condicoes.Count() > 0)
            {
                CmbCondicao.SelectedIndex = 0;
                CmbCondicao.IsEnabled = true;
            }
            else
            {
                CmbCondicao.IsEnabled = false;
            }

            Mouse.OverrideCursor = null;
        }

        private void ShowProduto()
        {
            if (CmbCliente.SelectedIndex < 0 || CmbCliente.SelectedIndex < 0) return;
            Mouse.OverrideCursor = Cursors.Wait;

            ProdutoRepository produtoRepository = new ProdutoRepository();

            var produtos = produtoRepository.BuscaProdutos(((Condicao)CmbCondicao.SelectedValue).CondicaoCodigo,
                                                           ((Filial)CmbFilial.SelectedValue).FilialCodigo,
                                                           ((Fornecedor)CmbLaboratorio.SelectedValue).FornecedorCodigo,
                                                           ((KeyValuePair<int, string>)CmbSituacaoProd.SelectedItem).Key,
                                                           ((KeyValuePair<int, string>)CmbEstoque.SelectedItem).Key)
                                                           .OrderBy(p=>p.Descricao);

            if (produtos.Count() > 1)
            {
                CmbProduto.ItemsSource = produtos;
                CmbProduto.SelectedIndex = 0;
                CmbProduto.IsEnabled = true;
            }
            else
            {
                CmbProduto.ItemsSource = null;
                CmbProduto.IsEnabled = false;
            }

            Mouse.OverrideCursor = null;
        }

        private void ShowEstoque()
        {
            CmbEstoque.ItemsSource = new Dictionary<int, string>
            {
                {0, "Com estoque"},
                {1, "Sem estoque"}
            };

            if (CmbEstoque.Items.Count > 0) CmbEstoque.SelectedIndex = 0;
        }

        private void ShowOrdenacao()
        {
            CmbOrdenacao.ItemsSource = new Dictionary<int, string>
            {
                {0, "Laboratório"},
                {1, "Produto"},
                {2, "Condição" }
            };

            if (CmbOrdenacao.Items.Count > 0) CmbOrdenacao.SelectedIndex = 0;
        }

        private void ShowSituacaoProd()
        {
            CmbSituacaoProd.ItemsSource = new Dictionary<int, string>
            {
                {999, "(Todos)"},
                {0, "Ativo"},
                {2, "Inativo"},
                {1, "Bloqueado"}
            };

            if (CmbSituacaoProd.Items.Count > 0) CmbSituacaoProd.SelectedIndex = 0;
        }

        private void ShowOrigemPed()
        {
            CmbOrigemPed.ItemsSource = new Dictionary<int, string>
            {
                {0,"CPD"},
                {1 ,"Televendas"},
                {2 ,"BBS"},
                {3 ,"Vendedor"},
                {4 ,"Balcao"},
                {5 ,"Internet"},
                {6 ,"TV - Varejo"},
                {7 ,"Checkout"},
                {8 ,"Operador Logistico"},
                {9 ,"EDI"},
                {10,"BBS - CD"},
                {11,"Cotacao"}
            };

            if (CmbOrigemPed.Items.Count > 0) CmbOrigemPed.SelectedIndex = 0;
        }

        private void ShowFornecedor(int? codProduto)
        {
            FornecedorRepository fornecedorRepository = new FornecedorRepository();
            var fornecedores = fornecedorRepository.ObterTodos();

            CmbLaboratorio.ItemsSource = fornecedores;

            if (CmbLaboratorio.Items.Count > 0) CmbLaboratorio.SelectedIndex = 0;
        }

        private void CmbCliente_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            Mouse.OverrideCursor = Cursors.Wait;
            if (CmbCliente.Text.Trim() == "") return;
            ShowCliente(CmbCliente.Text.Trim());
            Mouse.OverrideCursor = null;
            if (CmbCliente.Items.Count > 0) CmbCliente.SelectedIndex = 0;
        }

        private void CmbCliente_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ShowCondicao();
        }

        private void CmbCondicao_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            AtualizaDados();
        }

        private void CmbFilial_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            AtualizaDados();
        }

        private void CmbLaboratorio_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            AtualizaDados();
        }

        private void CmbEstoque_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            AtualizaDados();
        }

        private void CmbSituacaoProd_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            AtualizaDados();
        }

        private void CmbOrdenacao_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e) { }

        private void CmbOrigemPed_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ShowCondicao();
            AtualizaDados();
        }

        private void CmbProduto_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e) { }


        private void AtualizaDados()
        {
            Validacao();
            if (CmbCliente.SelectedItem != null && CmbCondicao.SelectedItem != null && _liberaAtualizacao)
            {
                ShowProduto();
                if (CmbProduto.Items.Count > 0)
                {
                    CmbProduto.IsEnabled = true;
                }
            }
        }

        private string Validacao()
        {
            if (CmbCliente.SelectedIndex < 0) return "Cliente não especificado!\n" + TxtLog.Text;

            return "";
        }

        private void LiberaItemTela(bool liberar)
        {
            CmbFilial.IsEnabled = liberar;
            CmbLaboratorio.IsEnabled = liberar;
            CmbOrdenacao.IsEnabled = liberar;
            CmbEstoque.IsEnabled = liberar;
            CmbSituacaoProd.IsEnabled = liberar;
            CmdGeraRelatorio.IsEnabled = liberar;
            CmdGeraRelatorio.IsEnabled = liberar;
            CmbOrigemPed.IsEnabled = liberar;
        }
    }
}