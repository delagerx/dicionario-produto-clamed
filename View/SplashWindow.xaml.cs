﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace RxRelatorioPreco.View
{
    /// <summary>
    /// Interaction logic for SplashWindow.xaml
    /// </summary>
    public partial class SplashWindow : Window
    {
        private Thread _loadingThread;
        private readonly Storyboard _showboard;
        private readonly Storyboard _hideboard;

        private delegate void ShowDelegate(string txt);

        private delegate void HideDelegate();

        private readonly ShowDelegate _showDelegate;
        private readonly HideDelegate _hideDelegate;

        public SplashWindow()
        {
            InitializeComponent();
            _showDelegate = new ShowDelegate(this.showText);
            _hideDelegate = new HideDelegate(this.hideText);
            _showboard = this.Resources["showStoryBoard"] as Storyboard;
            _hideboard = this.Resources["HideStoryBoard"] as Storyboard;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _loadingThread = new Thread(load);
            _loadingThread.Start();
        }

        private void load()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;

            this.Dispatcher.Invoke(_showDelegate, "Versão " + version);

            Thread.Sleep(2000);
            this.Dispatcher.Invoke(_showDelegate, "Versão " + version);
            Thread.Sleep(2000);
            //load data 
            this.Dispatcher.Invoke(_hideDelegate);

            //close the window
            Thread.Sleep(2000);
            this.Dispatcher.Invoke(DispatcherPriority.Normal,
                (Action) delegate() { Close(); });
        }

        private void showText(string txt)
        {
            txtLoading.Text = txt;
            BeginStoryboard(_showboard);
        }

        private void hideText()
        {
            BeginStoryboard(_hideboard);
        }
    }
}
