﻿namespace RxRelatorioPreco.Model
{
    public class PrecoItem
    {
        public int CodigoProduto { get; set; }
        public double Preco { get; set; }
        public double Pdu { get; set; }
        public string codCond { get; set; }
    }
}