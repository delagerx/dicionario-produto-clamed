﻿namespace RxRelatorioPreco.Model
{
    public class Condicao
    {
        public string CondicaoCodigo { get; set; }
        public string Descricao { get; set; }
        public int CondicaoId { get; set; }
    }
}