﻿namespace RxRelatorioPreco.Model
{
    public class Filial
    {
        public int FilialCodigo { get; set; }
        public string Descricao { get; set; }
        public string UF { get; set; }
    }
}