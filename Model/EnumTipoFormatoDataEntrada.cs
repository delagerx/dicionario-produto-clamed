﻿namespace RxRelatorioPreco.Model
{
    public enum EnumTipoFormatoDataEntrada
    {
        Cpd = 0,
        Televendas = 1,
        Bbs = 2,
        Vendedor = 3,
        Balcao = 4,
        Internet = 5,
        TvVarejo = 6,
        Checkout = 7,
        OperadorLogistico = 8,
        Edi = 9,
        BbsCd = 10,
        Cotacao = 11

    }
}