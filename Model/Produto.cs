﻿using System.Collections.Generic;

namespace RxRelatorioPreco.Model
{
    public class Produto
    {
        public int? ProdutoCodigo { get; set; }
        public int? Digito { get; set; }
        public string Descricao { get; set; }
        public string CodigoBarra { get; set; }
        public double Preco { get; set; }
        public double PrecoLiquido { get; set; }
        public int Estoque { get; set; }
        public TributacaoItem TributacaoItem { get; set; }
        public string Fornecedor { get; set; }
        public string Condicao { get; set; }
    }
}