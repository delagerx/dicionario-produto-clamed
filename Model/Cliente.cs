﻿namespace RxRelatorioPreco.Model
{
    public class Cliente
    {
        public int ClienteCodigo { get; set; }
        public string Descricao { get; set; }
        public int BandeiraCodigo { get; set; }

    }
}