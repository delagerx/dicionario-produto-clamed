﻿namespace RxRelatorioPreco.Model
{
    public class Fornecedor
    {
        public int? FornecedorCodigo { get; set; }
        public string Descricao { get; set; }
    }
}
