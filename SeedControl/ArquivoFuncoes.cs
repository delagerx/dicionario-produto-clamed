﻿using RxRelatorioPreco.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;


namespace RxRelatorioPreco.SeedControl
{
    public class ArquivoFuncoes
    {
        private static ArquivoFuncoes _arquivoFunctions = new ArquivoFuncoes();

        private static ProdutoFuncoes _produtoFunctions = new ProdutoFuncoes();

        private static Excel.Application xlApp;
        private static Excel.Workbook xlWorkBook;
        private static Excel.Worksheet xlWorkSheet;
        private static object misValue = System.Reflection.Missing.Value;


        public string BuscaPathArquivo()
        {
            var pathArquivo = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) + "";

            pathArquivo = pathArquivo.Remove(0, 6) + @"\Arquivo\";

            if (!Directory.Exists(pathArquivo)) Directory.CreateDirectory(pathArquivo);
            var path = pathArquivo;

            return path;
        }


        public Excel.Worksheet GravaLinhaArquivo(Produto produto, string codCond, Excel.Worksheet xlWorkSheet, int linha)
        {
            xlWorkSheet.Cells[linha, 1].Value = (produto.ProdutoCodigo).ToString() + (produto.Digito).ToString();
            xlWorkSheet.Cells[linha, 2].Value = produto.Descricao;
            xlWorkSheet.Cells[linha, 3].NumberFormat = "@";
            xlWorkSheet.Cells[linha, 3].Value = produto.CodigoBarra;
            xlWorkSheet.Cells[linha, 4].Value = produto.Fornecedor;
            xlWorkSheet.Cells[linha, 5].Value = produto.Estoque;
            xlWorkSheet.Cells[linha, 6].Value = Math.Round(produto.Preco, 2);
            xlWorkSheet.Cells[linha, 7].Value = Math.Round(produto.TributacaoItem.pdu, 2) + "%";
            xlWorkSheet.Cells[linha, 8].Value = Math.Round(produto.PrecoLiquido, 2);
            xlWorkSheet.Cells[linha, 9].Value = Math.Round(produto.TributacaoItem.valor_icms_retido, 2);
            xlWorkSheet.Cells[linha, 10].Value = Math.Round((produto.PrecoLiquido + produto.TributacaoItem.valor_icms_retido), 2);
            xlWorkSheet.Cells[linha, 11].Value = codCond;

            return xlWorkSheet;
        }


        public Excel.Worksheet MontaCabecalho(Excel.Worksheet xlWorkSheet)
        {
            var cabecalho = new string[]
           {
               "Código Produto",
               "Descrição",
               "EAN",
               "Razão Social",
               "Estoque Disponível",
               "Preço Fábrica",
               "Desconto",
               "Preço Líquido",
               "Substituição Tributária",
               "Valor Total",
               "Condição de Venda"
           };

            var i = 1;
            foreach (var item in cabecalho)
            {
                xlWorkSheet.Cells[1, i++].Value = item;
            }
            return xlWorkSheet;
        }


        public Excel.Workbook CriaArquivo()
        {
            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);

            xlWorkBook.Title = "Relatório de Preço";

            return xlWorkBook;
        }


        public Excel.Worksheet CriaAbaArquivo(Excel.Workbook xlWorkBook)
        {
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            xlWorkSheet = _arquivoFunctions.MontaCabecalho(xlWorkSheet);

            return xlWorkSheet;
        }


        public void SalvaArquivo(Excel.Workbook werkBook, string pathArquivo)
        {
            try
            {
                string nomeArquivo = "Relatório de Preço " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                werkBook.SaveAs((pathArquivo + nomeArquivo), Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue,
                Excel.XlSaveAsAccessMode.xlShared, misValue, misValue, misValue, misValue, pathArquivo);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                werkBook.Close(true, misValue, misValue);
                xlApp.Quit();
            }
        }


        public void ProcessaArquivo(IEnumerable<Produto> produtos)
        {
            var pathArquivo = BuscaPathArquivo();
            var xlWorkBook = CriaArquivo();
            var xlWorkSheet = CriaAbaArquivo(xlWorkBook);
            MontaCabecalho(xlWorkSheet);
            var count = 1;

            foreach (var produto in produtos)
            {
                count++;
                GravaLinhaArquivo(produto, produto.Condicao, xlWorkSheet, count);
                xlWorkSheet = _arquivoFunctions.GravaLinhaArquivo(produto, produto.Condicao, xlWorkSheet, count);
           
            }

            SalvaArquivo(xlWorkBook, pathArquivo);
        }

    }
}
