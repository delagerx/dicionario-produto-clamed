﻿
using System.Data.SqlClient;


namespace RxRelatorioPreco.SeedControl
{
    public abstract class BaseRepository
    {
        private static string connectionString = RxRelatorioPreco.Properties.Settings.Default.connectionString;
        protected SqlConnection _bd => new SqlConnection(connectionString);
    }
}