﻿using Excel = Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using System.Linq;
using RxRelatorioPreco.Model;
using RxRelatorioPreco.Repository;

namespace RxRelatorioPreco.SeedControl
{
    public class ProdutoFuncoes
    {


        public IEnumerable<Produto> OrdenaProdutoList(IEnumerable<Produto> produtos, int codOrdenacao)
        {
            switch (codOrdenacao)
            {
                case 0:
                    produtos = produtos.OrderBy(p => p.Fornecedor);
                    break;
                case 1:
                    produtos = produtos.OrderBy(p => p.Descricao);
                    break;
                case 2:
                    produtos = produtos.OrderBy(p => p.Condicao);
                    break;
            }
            return produtos;
        }


        public double CalculaPrecoLiquido(Produto produto)
        {
            return produto.Preco - (produto.Preco * (produto.TributacaoItem.pdu / 100));
        }

        public TributacaoItem CalculaTributacao(int? produtoCodigo, PrecoItem precoItem, int clienteCodigo, int filialCodigo)
        {
            TributacaoItem tributacaoItem;
            var tributationRepository = new TributationRepository();

            tributacaoItem = tributationRepository.BuscaTributacao(clienteCodigo, produtoCodigo, 0, filialCodigo);

            tributacaoItem.quantidade = 1;
            tributacaoItem.qb = 0;
            tributacaoItem.preco_fabrica = precoItem.Preco;
            tributacaoItem.pdu = precoItem.Pdu;
            tributacaoItem.pdf = 0;
            tributacaoItem.casa_decimal_unitaria = 2;
            tributacaoItem.casa_decimal_total = 4;
            tributacaoItem.pdp = 0;

            tributacaoItem.CalculaTributacao();

            return tributacaoItem;
        }


        public IEnumerable<Produto> BuscaProdutos(string codCond, int filialId, int? fornecedorId, int situacao, int estoque)
        {
            ProdutoRepository produtoRepository = new ProdutoRepository();

            var produtos = produtoRepository.BuscaProdutos(codCond,
                                                           filialId,
                                                           fornecedorId,
                                                           situacao,
                                                           estoque
                                                           );

            produtos = produtos.OrderBy(p => p.Descricao);

            return produtos;
        }


        public Produto MontaProduto(Produto produto, int clienteId, int filialId, int origemPed,
                                        int? condicaoId, string UF)
        {
            var _precoItemRepository = new PrecoItemRepository();
            var _produtoRepository = new ProdutoRepository();

            PrecoItem precoItem = null;

            if (condicaoId != null)
            {
                precoItem = _precoItemRepository.PegaPrecoItem(produto.ProdutoCodigo, condicaoId);
            }
            else
            {
                precoItem = _precoItemRepository.PegaMelhorDesconto(clienteId, produto.ProdutoCodigo, filialId,
                                                          origemPed, condicaoId, UF);
            }

            produto.Preco = precoItem.Preco;
            produto.Condicao = precoItem.codCond;
            var produtoTrib = CalculaTributacao(produto.ProdutoCodigo, precoItem, clienteId, filialId);
            produto.TributacaoItem = produtoTrib;
            produto.PrecoLiquido = CalculaPrecoLiquido(produto);


            return produto;
        }


    }
}
