﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using RxRelatorioPreco.Model;
using RxRelatorioPreco.SeedControl;

namespace RxRelatorioPreco.Repository
{
    public class ProdutoRepository : BaseRepository
    {

        public IEnumerable<Produto> BuscaProdutos(string codCond, int filial, int? codFornecedor,
                                               int? situacaoProd, int temEstoque)
        {
            IEnumerable<Produto> produtos = null;
            var parameters = new DynamicParameters();
            parameters.Add("filial", filial, DbType.Int32);

            var sql = @"SELECT DISTINCT p.cod_produto AS ProdutoCodigo, 
                                    p.digito as Digito,
                                    p.descricao as Descricao, 
                                    cf.cod_barra AS CodigoBarra, 
                                    0.00 AS Preco, 
                                    0.00 As PrecoLiquido,
                                    e.razao_social as Fornecedor,
                                    pe.estoque as Estoque
                                FROM produto p 
                                    INNER JOIN caixa_fechada cf (NOLOCK) ON cf.cod_produto = p.cod_produto 
                                                                   AND cf.principal = -1 
                                    INNER JOIN	produto_preco_venda pcv (NOLOCK) ON pcv.cod_produto = p.cod_produto 
                                    INNER JOIN	def_pis_cofins dpc (NOLOCK) ON dpc.cod_def_pis_cofins = p.pis_cofins
								    INNER JOIN  cond_prod cpr (NOLOCK) ON cpr.cod_produto =p.cod_produto
								    INNER JOIN  produto_estoque pe (NOLOCK) ON pe.cod_produto = p.cod_produto
                                    INNER JOIN  entidade e (NOLOCK) ON e.cod_entidade = p.cod_entidade
								    LEFT JOIN   nf_tributacao nt (NOLOCK) ON nt.cod_preco_despesa_acessoria = pcv.cod_preco                           
                                WHERE pe.cod_filial = @filial";


            if (codFornecedor != null)
            {
                parameters.Add("codFornecedor", codFornecedor, DbType.Int32);
                sql = sql + " AND p.cod_entidade = @codFornecedor";
            }

            if (codCond != "(Todos)")
            {
                parameters.Add("codCond", codCond, DbType.String);
                sql = sql + " AND cpr.cod_cond = @codCond";
            }

            if (situacaoProd != 999)
            {
                parameters.Add("situacaoProd", situacaoProd, DbType.Int32);
                sql = sql + " AND p.situacao = @situacaoProd";
            }

            if (temEstoque == 0) sql = sql + " AND (pe.estoque - retencao) > 0";
            else sql = sql + " AND (pe.estoque - retencao) <= 0";

            sql = sql + " UNION";
            sql = sql + @" SELECT DISTINCT null AS ProdutoCodigo, 
                                    null AS Digito,
                                    '(Todos)' AS Descricao, 
                                    null AS CodigoBarra, 
                                    0.00 AS Preco,
                                    0.00 AS PrecoLiquido,
                                    null AS Fornecedor,
                                    null AS Estoque";

            using (_bd)
            {
                produtos = _bd.Query<Produto>(sql, parameters);
            }

            return produtos;
        }


        public Produto BuscaProdutoById(int? codProduto, int filial, string codCond)
        {
            Produto produto = null;
            var parameters = new DynamicParameters();
            parameters.Add("filial", filial, DbType.Int32);
            parameters.Add("codProduto", codProduto, DbType.Int32);

            var sql = @"SELECT DISTINCT p.cod_produto AS ProdutoCodigo, 
                                    p.digito as Digito,
                                    p.descricao as Descricao, 
                                    cf.cod_barra AS CodigoBarra, 
                                    0.00 AS Preco, 
                                    0.00 As PrecoLiquido,
                                    e.razao_social as Fornecedor,
                                    pe.estoque as Estoque,
                                    cpr.cod_cond as Condicao
                                FROM produto p 
                                    INNER JOIN caixa_fechada cf (NOLOCK) ON cf.cod_produto = p.cod_produto 
                                                                   AND cf.principal = -1 
                                    INNER JOIN	produto_preco_venda pcv (NOLOCK) ON pcv.cod_produto = p.cod_produto 
                                    INNER JOIN	def_pis_cofins dpc (NOLOCK) ON dpc.cod_def_pis_cofins = p.pis_cofins
								    INNER JOIN  cond_prod cpr (NOLOCK) ON cpr.cod_produto =p.cod_produto
								    INNER JOIN  produto_estoque pe (NOLOCK) ON pe.cod_produto = p.cod_produto
                                    INNER JOIN  entidade e (NOLOCK) ON e.cod_entidade = p.cod_entidade
								    LEFT JOIN   nf_tributacao nt (NOLOCK) ON nt.cod_preco_despesa_acessoria = pcv.cod_preco                           
                                WHERE pe.cod_filial = @filial
                                    AND p.cod_produto = @codProduto";

            if (codCond != null)
            {
                parameters.Add("codCond", codCond,DbType.String);
                sql += " AND cpr.cod_cond = @codCond";
            }

            using (_bd)
            {
                produto = _bd.QueryFirstOrDefault<Produto>(sql, parameters);
            }

            return produto;
        }


    }
}
