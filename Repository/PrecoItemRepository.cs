﻿using Dapper;

using RxRelatorioPreco.Model;
using RxRelatorioPreco.SeedControl;
using System.Data;

namespace RxRelatorioPreco.Repository
{
    public class PrecoItemRepository : BaseRepository
    {
        public PrecoItem PegaPrecoItem(int? idProduto, int? CondId)
        {
            PrecoItem precoItem = null;
            var parameters = new DynamicParameters();
            parameters.Add("idProduto", idProduto, DbType.Int32);
            parameters.Add("CondId", CondId, DbType.Int32);

            var sql = @"SELECT p.cod_produto as CodigoProduto,
                                cp.desconto as Pdu,
                                ISNULL(p.preco,0) as Preco ,
                                c.cod_cond as codCond
                        FROM produto p
							 inner join cond_prod cp (NOLOCK) on p.cod_produto = cp.cod_produto
							 inner join cond c (NOLOCK) on cp.cod_cond = c.cod_cond
                        WHERE c.id_cond = @CondId 
                             AND cp.cod_produto = @idProduto";

            using (_bd)
            {
                precoItem = _bd.QueryFirstOrDefault<PrecoItem>(sql, parameters);
            }

            return precoItem;
        }



        public PrecoItem PegaMelhorDesconto(int clienteCodigo, int? produtoCodigo,int filialCodigo,
                                            int? codigoOrigemPedido,int? condicaoId,string UF) 
        {

            PrecoItem precoItem = null;
            var parameters = new DynamicParameters();
            parameters.Add("clienteCodigo", clienteCodigo, DbType.Int32);
            parameters.Add("produtoCodigo", produtoCodigo, DbType.Int32);
            parameters.Add("filialCodigo", filialCodigo, DbType.Int32);
            parameters.Add("codigoOrigemPedido", codigoOrigemPedido, DbType.Int32);
            parameters.Add("condicaoId", condicaoId, DbType.Int32);
            parameters.Add("UF", UF, DbType.String);

            var sql = @"exec stp_g_melhor_cond_prod_desc_le_rel
                                            @clienteCodigo, @produtoCodigo, 
                                            @filialCodigo, @codigoOrigemPedido,
                                            1, @condicaoId, @UF";

            using (_bd)
            {
                precoItem = _bd.QueryFirstOrDefault<PrecoItem>(sql,parameters);
            }

            return precoItem;
        }
    }
}
