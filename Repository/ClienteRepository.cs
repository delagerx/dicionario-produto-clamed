﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using RxRelatorioPreco.Model;
using RxRelatorioPreco.SeedControl;

namespace RxRelatorioPreco.Repository
{
    public class ClienteRepository : BaseRepository
    {
        public Cliente Obter(string cli)
        {
            Cliente cliente = null;
            var parameters = new DynamicParameters();
            parameters.Add("cli", cli, DbType.String);
         

            var sql = $@"SELECT cod_entidade AS ClienteCodigo, 
                            razao_social + ' (' + fantasia + ')' AS Descricao, 
                            cod_bandeira AS BandeiraCodigo
                        FROM entidade (NOLOCK), param_entidade (NOLOCK) 
                        WHERE tipo = cod_cliente 
                            AND situacao = 0
                            AND (razao_social like (@cli) OR fantasia like (@cli)";

            int aux;
            if (int.TryParse(cli, out aux))
            {
                sql = sql + "OR (cod_entidade * 10 + digito) = '" + aux + "'";
            }

            sql = sql + ") Order by Descricao";


            using (_bd)
            {
                cliente = _bd.QueryFirstOrDefault<Cliente>(sql, parameters);
            }

            return cliente;
        }

    }
}