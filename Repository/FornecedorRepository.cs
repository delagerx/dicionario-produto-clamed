﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using RxRelatorioPreco.Model;
using RxRelatorioPreco.SeedControl;

namespace RxRelatorioPreco.Repository
{
    public class FornecedorRepository : BaseRepository
    {
        public IEnumerable<Fornecedor> ObterTodos()
        {
            IEnumerable<Fornecedor> fornecedores = null;

            var sql = @"SELECT Distinct NULL as FornecedorCodigo, '(Todos)' as Descricao 
                        UNION 
                        SELECT Distinct  prod.cod_entidade as FornecedorCodigo, forn.razao_social as Descricao 
                        FROM produto as prod (NOLOCK) 
                             INNER JOIN entidade as forn (NOLOCK) ON prod.cod_entidade = forn.cod_entidade
                        Order by Descricao";

            using (_bd)
            {
                fornecedores = _bd.Query<Fornecedor>(sql);
            }

            return fornecedores;
        }

        public Fornecedor ObterPorId(int? idProduto)
        {
            var parameters = new DynamicParameters();
            parameters.Add("idProduto", idProduto,DbType.Int32);

            Fornecedor fornecedor = null;

            var sql = @"SELECT Distinct prod.cod_entidade as FornecedorCodigo, 
                                        forn.razao_social as Descricao 
                        FROM produto AS prod (NOLOCK) 
                             INNER JOIN entidade AS forn (NOLOCK) ON prod.cod_entidade = forn.cod_entidade
                        WHERE prod.cod_produto = @idProduto
                        ORDER BY Descricao";

            using (_bd)
            {
                fornecedor = _bd.QueryFirstOrDefault<Fornecedor>(sql,parameters);
            }

            return fornecedor;
        }
    }
}