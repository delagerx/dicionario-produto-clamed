﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using RxRelatorioPreco.Model;
using RxRelatorioPreco.SeedControl;

namespace RxRelatorioPreco.Repository
{
    public class CondicaoRepository : BaseRepository
    {
        public IEnumerable<Condicao> SqlObterPorEntidade(int? codigoEntidade, int? origemPed)
        {
            IEnumerable<Condicao> condicoes = null;
            var parameters = new DynamicParameters();
            parameters.Add("codigoEntidade", codigoEntidade, DbType.Int32);
            parameters.Add("origemPed", origemPed, DbType.Int32);

            var sql = $@"SELECT DISTINCT c.cod_cond AS CondicaoCodigo, c.descricao AS Descricao, " +
                                "c.id_cond AS CondicaoId " +
                      "FROM cond c(NOLOCK) " +
                      "INNER JOIN cond_cliente cc (NOLOCK) " +
                                "ON c.cod_cond = cc.cod_cond " +
                      " WHERE cc.cod_entidade = @codigoEntidade" +
                      " AND c.final > GETDATE()";

            sql = sql + @" AND (C.vendedor = -1 OR ISNULL(@origemPed,-1) <> 0)    
                               AND (C.televendas = -1 OR ISNULL(@origemPed,-1) <> 1)    
                               AND (C.BBS = -1 OR ISNULL(@origemPed,-1) <> 2)    
                               AND (C.palm = -1 OR ISNULL(@origemPed,-1) <> 3)    
                               AND (C.balcao = -1 OR ISNULL(@origemPed,-1) <> 4)    
                               AND (C.internet = -1 Or ISNULL(@origemPed,-1) <> 5)    
                               AND (C.operador_logistico = -1 OR ISNULL(@origemPed,-1) <> 8)  
                               AND (C.cotacao = -1 OR ISNULL(@origemPed, -1) <> 11)";

            sql += " UNION " +
                 " SELECT '(Todos)' AS CondicaoCodigo, null AS Descricao, null AS CondicaoId";


            using (_bd)
            {
                condicoes = _bd.Query<Condicao>(sql, parameters);
            }

            return condicoes;
        }
    }
}