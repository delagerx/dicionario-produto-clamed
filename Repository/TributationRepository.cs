﻿using System;
using System.Data;
using Dapper;
using Delage.RX.RXCOMM;

using RxRelatorioPreco.Model;
using RxRelatorioPreco.SeedControl;

namespace RxRelatorioPreco.Repository
{
    public class TributationRepository : BaseRepository
    {
        public TributacaoItem BuscaTributacao(int? clienteCodigo, int? produtoCodigo, int operacao, int filialCodigo)
        {
            var tributacaoItem = new TributacaoItem();
            var parameters = new DynamicParameters();

            if (clienteCodigo != null) parameters.Add("clienteCodigo", clienteCodigo, DbType.Int32);
            else parameters.Add("clienteCodigo", null, DbType.String);

            if (produtoCodigo != null) parameters.Add("produtoCodigo", produtoCodigo, DbType.Int32);
            else parameters.Add("produtoCodigo", null, DbType.String);

            parameters.Add("operacao", operacao, DbType.Int32);
            parameters.Add("filialCodigo", filialCodigo,DbType.Int32);

            var sql = @"stp_tributacao_le @clienteCodigo, @produtoCodigo, @operacao, @filialCodigo";

            using (var reader = _bd.ExecuteReader(sql,parameters))
            {
                if(reader.Read())
                {
                    tributacaoItem.recolhimento = (Estruturas.TipoRecolhimento)Enum.Parse(typeof(Estruturas.TipoRecolhimento), reader["recolhimento"].ToString());
                    tributacaoItem.preco_nf = (Estruturas.TipoPreco_nf)Enum.Parse(typeof(Estruturas.TipoPreco_nf), reader["preco_nf"].ToString());
                    tributacaoItem.Tipodp = Estruturas.TipoDescProm.Desconto;
                    tributacaoItem.repasseicms = double.Parse(reader["repasseicms"].ToString());
                    tributacaoItem.utiliza_ipi = (reader["utiliza_IPI"].ToString() == "0") ? false : true;
                    tributacaoItem.tipo_ipi = (Estruturas.TipoIPI)Enum.Parse(typeof(Estruturas.TipoIPI), reader["tipo_ipi"].ToString());
                    tributacaoItem.peso = double.Parse(reader["peso"].ToString());
                    tributacaoItem.ipi = double.Parse(reader["ipi"].ToString());
                    tributacaoItem.utiliza_servico_icms = (reader["utiliza_servico_icms"].ToString() == "0") ? false : true;
                    tributacaoItem.utiliza_ipi_icms = (reader["utiliza_ipi_icms"].ToString() == "0") ? false : true;
                    tributacaoItem.preco_base_icms = double.Parse(reader["preco_base_icms"].ToString());
                    tributacaoItem.perredbaseicms = double.Parse(reader["perredbaseicms"].ToString());
                    tributacaoItem.perredvaloricms = double.Parse(reader["perredvaloricms"].ToString());
                    tributacaoItem.aliquotaicms = double.Parse(reader["AliquotaIcms"].ToString());
                    tributacaoItem.utiliza_servico_icms_retido = (reader["utiliza_servico_icms_retido"].ToString() == "0") ? false : true;
                    tributacaoItem.utiliza_ipi_icms_retido = (reader["utiliza_ipi_icms_retido"].ToString() == "0") ? false : true;
                    tributacaoItem.base_ret_desc = (reader["base_ret_desc"].ToString() == "0") ? false : true;
                    tributacaoItem.base_ret_rep = (reader["base_ret_rep"].ToString() == "0") ? false : true;
                    tributacaoItem.preco_base_icms_retido = double.Parse(reader["preco_base_icms_retido"].ToString());
                    tributacaoItem.preco_base_icms_icms_retido = double.Parse(reader["preco_base_icms_icms_retido"].ToString());
                    tributacaoItem.baseicmsret = double.Parse(reader["BaseIcmsRet"].ToString());
                    tributacaoItem.aliquotaicmsret = double.Parse(reader["aliquotaicmsret"].ToString());
                    tributacaoItem.grava_icmsret_no_areter = (reader["grava_icmsret_no_areter"].ToString() == "0") ? false : true;
                    tributacaoItem.nao_subtrai_icms = (reader["nao_subtrai_icms"].ToString() == "0") ? false : true;
                    tributacaoItem.Redbaseicmsret = double.Parse(reader["RedutorBaseIcmsRet"].ToString());
                    tributacaoItem.pis = double.Parse(reader["Aliq_pis_saida"].ToString());
                    tributacaoItem.cofins = double.Parse(reader["Aliq_Cofins_saida"].ToString());
                    tributacaoItem.ImpIcms_total = int.Parse(reader["ImpIcms_total"].ToString());
                    tributacaoItem.ImpIcms_obs = int.Parse(reader["ImpIcms_obs"].ToString());
                    tributacaoItem.ImpIcmsRet_total = int.Parse(reader["ImpIcmsRet_total"].ToString());
                    tributacaoItem.ImpIcmsRet_obs = int.Parse(reader["ImpIcmsRet_obs"].ToString());
                    tributacaoItem.id_natureza = int.Parse(reader["id_natureza"].ToString());
                    tributacaoItem.cod_def_pis_cofins = int.Parse(reader["cod_def_pis_cofins"].ToString());
                    tributacaoItem.base_ret_desc_fin = (reader["base_ret_desc_fin"].ToString() == "0") ? false : true;
                    tributacaoItem.base_desc_fin = (reader["base_desc_fin"].ToString() == "0") ? false : true;
                    tributacaoItem.subtrai_icms_valornf = (reader["subtrai_icms_total_NF"].ToString() == "0") ? false : true;
                    tributacaoItem.utiliza_bonific_icms = (reader["utiliza_bonific_icms"].ToString() == "0") ? false : true;
                    tributacaoItem.utiliza_bonific_icms_retido = (reader["utiliza_bonific_icms_retido"].ToString() == "0") ? false : true;
                    tributacaoItem.idtrib = int.Parse(reader["id_tributacao"].ToString());
                    tributacaoItem.formula_despesa_acessoria = reader["formula_despesa_acessoria"].ToString();
                    tributacaoItem.formula_despesa_acessoria = tributacaoItem.formula_despesa_acessoria.Replace(",", ".");
                }
                reader.Close();
            }

            return tributacaoItem;
        }        
    }
}