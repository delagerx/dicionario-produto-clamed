﻿using System.Collections.Generic;
using Dapper;
using RxRelatorioPreco.Model;
using RxRelatorioPreco.SeedControl;

namespace RxRelatorioPreco.Repository
{
    public class FilialRepository : BaseRepository
    {
        public IEnumerable<Filial> ObterTodos()
        {
            IEnumerable<Filial> filiais = null;

            var sql = @"SELECT f.cod_filial AS FilialCodigo, 
                            e.fantasia AS Descricao,
                            e.uf as UF 
                        FROM filial f (NOLOCK) 
						    INNER join vw_rs_entidade e (NOLOCK) ON f.cod_filial = e.id_entidade
                        ORDER BY e.fantasia";

            using (_bd)
            {
                filiais = _bd.Query<Filial>(sql);
            }

            return filiais;
        }
    }
}